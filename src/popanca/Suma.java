package popanca;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JTextField;


public class Suma extends JFrame{



    /**
     *
     */
    private static final long serialVersionUID = 1L;
    JLabel c1;
    JLabel c2;
    JTextField tbox1;
    JTextField tbox2;
    JTextField tbox3;
    JTextArea tbox4;
    JButton btn;

    Suma(){



        setTitle("Show Content");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(250,300);
        setVisible(true);
    }

    public void init(){





        this.setLayout(null);
        int width=100;int height = 20;

        c1 = new JLabel("Operand 1 ");
        c1.setBounds(10, 10, width, height);

        c2 = new JLabel("Operand 2 ");
        c2.setBounds(10, 40, width, height);


        tbox1 = new JTextField();
        tbox1.setBounds(90,10,width, height);

        tbox2 = new JTextField();
        tbox2.setBounds(90,40,width, height);

        tbox4 = new JTextArea();
        tbox4.setEditable(false);
        tbox4.setBounds(90,140,width, height);


        btn = new JButton("Calculeaza");
        btn.setBounds(90,100,width, height);

        btn.addActionListener(new TratareButon());


        add(c1); 
        add(c2);
        add(tbox1);
        add(tbox2);
        add(tbox4);
        add(btn);


    }

    public static void main(String[] args) {
        new Suma();
    }

    class TratareButon implements ActionListener{

        public void actionPerformed(ActionEvent e) {

            String a = Suma.this.tbox1.getText();
            String b = Suma.this.tbox2.getText();

            int i=Integer.parseInt(a);
            int j=Integer.parseInt(b);

            tbox4.setText(String.valueOf(i + j));
        }
    }
}